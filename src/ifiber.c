# include <fiber.h>

fiber_errcode_t
fiber_get(fiber_t *)
{
    return FIBER_ERROR;
}

fiber_errcode_t
fiber_set(fiber_t *)
{
    return FIBER_ERROR;
}

fiber_errcode_t
fiber_dup(fiber_t *, fiber_t *)
{
    return FIBER_ERROR;
}

fiber_errcode_t
fiber_make(fiber_t *)
{
    return FIBER_ERROR;
}

fiber_errcode_t
fiber_swap(fiber_t *, fiber_t *)
{
    return FIBER_ERROR;
}
