# include <assert.h>    /* assert */
# include <string.h>    /* memcpy */

# include "fiber.h"

inline fiber_errcode_t
fiber_dup(fiber_t * f1, fiber_t * f2)
{
    memcpy(f2, f1, sizeof(fiber_t));
    return FIBER_SUCCESS;
}

inline fiber_errcode_t
fiber_make(fiber_t * f)
{
    assert(f->entry);
    assert(f->stack.ptr);
    assert(f->stack.size);

    uintptr_t * sp = (uintptr_t *) ((uintptr_t)f->stack.ptr + f->stack.size - 1);
    f->regs.values[REG_CPU_RIP] = (uintptr_t) f->entry;
    f->regs.values[REG_CPU_RSP] = (uintptr_t) sp;

    return FIBER_SUCCESS;
}
