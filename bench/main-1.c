#include <fiber.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>

#define N 1e5

static fiber_t f1, f2;
static ucontext_t ctx1, ctx2;

static uint64_t c0, c1, c2, c3;
static uint64_t cycles, cycles_func, cycles_make, cycles_swap, cycles_set;

static double t0, dt;

uint64_t
rdtsc(void)
{
    uint32_t lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return (uint64_t)hi << 32 | lo;
}

static void
fiber_main(void)
{
    c2 = rdtsc();
    fiber_set(&f2);
}

static void
ucontext_main(void)
{
    c2 = rdtsc();
    setcontext(&ctx2);
}

static void
f(void) {}

int
main(void)
{
    {
        char stack[8192];

        // function call
        t0 = omp_get_wtime();
        {
            for (int n = 0 ; n < N ; ++n)
            {
                c0 = rdtsc();
                f();
                c1 = rdtsc();
                cycles_func += c1 - c0;
                cycles += rdtsc() - c0;
            }
            dt = omp_get_wtime() - t0;
            printf("function call took %lf s. for %lu cyc and %.3lf cyc/s. in avg.\n",
                dt, (uint64_t)(cycles_func / N), cycles / dt);
        }

        // fiber version
        t0 = omp_get_wtime();
        cycles = 0;
        {
            for (int n = 0 ; n < N ; ++n)
            {
                c0 = rdtsc();
                f1.entry        = fiber_main;
                f1.stack.ptr    = (void *) stack;
                f1.stack.size   = sizeof(stack);

                if (fiber_make(&f1) != FIBER_SUCCESS)
                {
                    fprintf(stderr, "Error making fiber\n");
                    exit(-1);
                }
                c1 = rdtsc();
                if (fiber_swap(&f2, &f1) != FIBER_SUCCESS)
                {
                    fprintf(stderr, "Error swapping fibers\n");
                    exit(-1);
                }
                c3 = rdtsc();
                cycles_make += c1 - c0;
                cycles_swap += c2 - c1;
                cycles_set += c3 - c2;
                cycles += rdtsc() - c0;
            }
        }
        dt = omp_get_wtime() - t0;
        printf("fiber took %lf s. (cpu freq avg = %.3lf cyc/s.)\n",
            dt, cycles / dt);
        printf("   make in %lu cycles\n", (uint64_t)(cycles_make / N));
        printf("   swap in %lu cycles\n", (uint64_t)(cycles_swap / N));
        printf("   set in %lu cycles\n", (uint64_t)(cycles_set / N));

        // ucontext version
        t0 = omp_get_wtime();
        cycles = 0;
        cycles_make = 0;
        cycles_swap = 0;
        cycles_set = 0;
        {
            for (int n = 0 ; n < N ; ++n)
            {
                c0 = rdtsc();
                if (getcontext(&ctx1) == -1)
                {
                    fprintf(stderr, "Error on getcontext\n");
                    exit(-1);
                }
                ctx1.uc_stack.ss_sp = stack;
                ctx1.uc_stack.ss_size = sizeof(stack);
                ctx1.uc_stack.ss_flags = 0;
                makecontext(&ctx1, ucontext_main, 0);
                c1 = rdtsc();
                if (swapcontext(&ctx2, &ctx1) != 0)
                {
                    fprintf(stderr, "Error swapping contextes\n");
                    exit(-1);
                }
                c3 = rdtsc();
                cycles_make += c1 - c0;
                cycles_swap += c2 - c1;
                cycles_set += c3 - c2;
                cycles += rdtsc() - c0;
            }
        }
        dt = omp_get_wtime() - t0;
        printf("ucontext took %lf s. (cpu freq avg = %.3lf cyc/s.)\n", dt, cycles / dt);
        printf("   make in %lu cycles\n", (uint64_t)(cycles_make / N));
        printf("   swap in %lu cycles\n", (uint64_t)(cycles_swap / N));
        printf("   set in %lu cycles\n", (uint64_t)(cycles_set / N));
    }

    // ucontext version

    return 0;
}
