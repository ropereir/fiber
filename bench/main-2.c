# define _GNU_SOURCE
# include <sched.h>

# include <pthread.h>
# include <stdio.h>
# include <stdint.h>
# include <unistd.h>
# include <sys/time.h>

# define SLEEP_TIME 10 /* in sec */
# define WORK_TIME  10 /* in sec */
# define CPU_ID 0

static pthread_t t1, t2;
static uint64_t c1, c2;
static int fds[2];

static double
get_time(void)
{
    struct timeval tp;
    gettimeofday (&tp, NULL);
    return tp.tv_sec + tp.tv_usec * 1e-6;
}

# define  INCR1(C) ++(C)
# define  INCR2(C) INCR1(C);INCR1(C);
# define  INCR4(C) INCR2(C);INCR2(C);
# define  INCR8(C) INCR4(C);INCR4(C);
# define INCR16(C) INCR8(C);INCR8(C);
# define INCR32(C) INCR16(C);INCR16(C);
# define INCR64(C) INCR32(C);INCR32(C);

static inline void
increment(uint64_t * c)
{
    double t0 = get_time();
    while (get_time() - t0 < WORK_TIME)
    {
        INCR64(*c);
    }
}

static void
bind_to_core(int cpu_id)
{
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(cpu_id, &cpuset);
    pthread_t current_thread = pthread_self();
    pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
#if 0
   sched_setaffinity(0, sizeof(cpu_set_t), &cpuset);
#endif
}

static void *
main_thread_1(void * unused)
{
    bind_to_core(CPU_ID);
    usleep(1e6 * SLEEP_TIME);
    // sleep(SLEEP_TIME);
    //struct timespec req, rem;
    //double remains;
    //req.tv_sec = (uint64_t) SLEEP_TIME;
    //req.tv_nsec = (SLEEP_TIME - (uint64_t)SLEEP_TIME) * 1e9;
    //clock_nanosleep(CLOCK_REALTIME, 0, &req, &rem);
    //remains = rem.tv_sec + rem.tv_nsec * 1e-9;
    //printf("remains to sleep %lf\n", remains);
    increment(&c1);
    return unused;
}

static void *
main_thread_2(void * unused)
{
    bind_to_core(CPU_ID);
    double t0 = get_time();
    while (get_time() - t0 < SLEEP_TIME);
    increment(&c2);
    return unused;
}

int
main(void)
{
    pipe(fds);
    pthread_create(&t1, NULL, main_thread_1, NULL);
    pthread_create(&t2, NULL, main_thread_2, NULL);
    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    printf("c1=%lu, c2=%lu, diff is %ld <=> %.2lf%%\n", c1, c2, c1 - c2, (c1 - c2) / (double) c1 * 100.0);
    return 0;
}

