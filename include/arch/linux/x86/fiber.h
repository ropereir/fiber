#ifndef __LINUX_X86_FIBER_H__
# define __LINUX_X86_FIBER_H__

enum
{
    /* cpu registers */
    REG_CPU_R8      = 0,
    REG_CPU_R9      = 1,
    REG_CPU_R10     = 2,
    REG_CPU_R11     = 3,
    REG_CPU_R12     = 4,
    REG_CPU_R13     = 5,
    REG_CPU_R14     = 6,
    REG_CPU_R15     = 7,
    REG_CPU_RBP     = 8,
    REG_CPU_RIP     = 9,
    REG_CPU_RSP     = 10,
    REG_CPU_RDI     = 11,
    REG_CPU_RSI     = 12,
    REG_CPU_RAX     = 13,
    REG_CPU_RBX     = 14,
    REG_CPU_RCX     = 15,
    REG_CPU_RDX     = 16,
    REG_CPU_EFLAGS  = 17,
    REG_CPU_SS      = 19,
    REG_CPU_CS      = 18,
    REG_CPU_DS      = 20,
    REG_CPU_ES      = 21,
    REG_CPU_FS      = 22,
    REG_CPU_GS      = 23,

    /* fpu registers */
    /* control word, status word, tag word, instruction pointer, data pointer, and last opcode */
    REG_FPU_CW_SW_TW    = 24, /* 16 bits padding */
    REG_FPU_RIP         = 25,
    REG_FPU_RDP         = 26,
    REG_FPU_LOP         = 27,

    NREGS = 28,
};

#endif /* __LINUX_X86_FIBER_H__ */
