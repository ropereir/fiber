# define oR8  24
# define oR9  32
# define oR10 40
# define oR11 48
# define oR12 56
# define oR13 64
# define oR14 72
# define oR15 80

# define oRBP 88
# define oRIP 96
# define oRSP 104

# define oRDI 112
# define oRSI 120

# define oRAX 128
# define oRBX 136
# define oRCX 144
# define oRDX 152

# define oEFL 160
# define oSS  168
# define oCS  176
# define oDS  184
# define oES  192
# define oFS  200
# define oGS  208

# define oFPU_REGS  216
# define oFPU_CW    216
# define oFPU_SW    218
# define oFPU_TW    220
# define oFPU_PAD16 222

# define oFPU_RIP   224
# define oFPU_RDP   232
# define oFPU_LOP   240
